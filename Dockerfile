FROM ubuntu:20.04
MAINTAINER trux <trux@truxton.com>

## to build :
##   docker build --no-cache -f Dockerfile -t bosfs:latest .
## to run :
##   docker run --rm --privileged --net=host --device=/dev/fuse --name bosfs -h bosfs -v `/bin/pwd`:/mnt/bosfs.git --dns 1.0.0.1 bosfs:latest /init.sh

## --privileged might not be necessary if you uncomment #user_allow_other from /etc/fuse.conf on the host

# Do not exclude man pages & other documentation
RUN bash -c "rm -vf /etc/dpkg/dpkg.cfg.d/excludes"
RUN bash -c "rm -rf /var/lib/apt/lists/*"
RUN bash -c "apt-get update -yq"
RUN bash -c "yes | unminimize"

RUN { \
      echo '#!/bin/bash' ; \
      echo ; \
      echo 'export TZ="/usr/share/zoneinfo/America/Los_Angeles"' ; \
      echo 'export LC_ALL="C"' ; \
      echo 'export DEBIAN_FRONTEND=noninteractive' ; \
      echo 'umask 0022' ; \
      echo 'echo "================================= : " $1' ; \
      echo ; \
      echo 'apt-get install -yq --ignore-missing $1' ; \
      echo 'true' ; \
      echo ; \
    } > /robust_install.sh && chmod a+rx /robust_install.sh

RUN { \
  /robust_install.sh apt-utils ; \
  /robust_install.sh dialog ; \
  /robust_install.sh libncurses5-dev ; \
  /robust_install.sh libreadline-dev ; \
  /robust_install.sh locales ; \
  /robust_install.sh locales-all ; \
  /robust_install.sh tzdata ; \
  /robust_install.sh aptitude ; \
  /robust_install.sh emacs-nox ; \
  /robust_install.sh screen ; \
  /robust_install.sh rsync ; \
  /robust_install.sh openssh-server ; \
  /robust_install.sh less ; \
  /robust_install.sh man ; \
  /robust_install.sh manpages-posix ; \
  /robust_install.sh vim ; \
  /robust_install.sh git ; \
  /robust_install.sh git-lfs ; \
  /robust_install.sh build-essential ; \
  /robust_install.sh subversion ; \
  /robust_install.sh bison ; \
  /robust_install.sh flex ; \
  /robust_install.sh tcsh ; \
  /robust_install.sh apcalc ; \
  /robust_install.sh bc ; \
  /robust_install.sh autoconf ; \
  /robust_install.sh automake ; \
  /robust_install.sh libtool ; \
  /robust_install.sh sudo ; \
  /robust_install.sh gdb ; \
  /robust_install.sh valgrind ; \
  /robust_install.sh strace ; \
  /robust_install.sh sysstat ; \
  /robust_install.sh psmisc ; \
  /robust_install.sh iotop ; \
  /robust_install.sh cmake ; \
  /robust_install.sh curl ; \
  /robust_install.sh wget ; \
  /robust_install.sh samba ; \
  /robust_install.sh wipe ; \
  /robust_install.sh zip ; \
  /robust_install.sh smbclient ; \
  /robust_install.sh net-tools ; \
  /robust_install.sh moreutils ; \
  /robust_install.sh pv ; \
  /robust_install.sh libssl-dev ; \
  /robust_install.sh python ; \
  /robust_install.sh libpython-dev ; \
  /robust_install.sh python-virtualenv ; \
  /robust_install.sh expect ; \
  /robust_install.sh netpbm ; \
  /robust_install.sh jq ; \
  /robust_install.sh units ; \
  /robust_install.sh inotify-tools ; \
  /robust_install.sh netcat-traditional ; \
  /robust_install.sh telnet ; \
  /robust_install.sh rsyslog ; \
  /robust_install.sh udev ; \
  /robust_install.sh libcurl4-openssl-dev ; \
  /robust_install.sh libldap2-dev ; \
  /robust_install.sh iputils-ping ; \
  /robust_install.sh nasm ; \
  /robust_install.sh libpq-dev ; \
  /robust_install.sh libcrypto++-dev ; \
  /robust_install.sh libxerces-c-dev ; \
  /robust_install.sh unionfs-fuse ; \
  /robust_install.sh sshfs ; \
  /robust_install.sh libtool-bin ; \
  /robust_install.sh libpopt-dev ; \
  /robust_install.sh libc6:i386 ; \
  /robust_install.sh linux-libc-dev:i386 ; \
  /robust_install.sh libc6-dev-i386 ; \
  /robust_install.sh gcc ; \
  /robust_install.sh g++ ; \
  /robust_install.sh gcc-multilib ; \
  /robust_install.sh g++-multilib ; \
  /robust_install.sh libpoco-dev ; \
  /robust_install.sh libpoco-dev:i386 ; \
  /robust_install.sh libboost-date-time-dev ; \
  /robust_install.sh libboost-thread-dev ; \
  /robust_install.sh libboost-system-dev ; \
  /robust_install.sh libkrb5-dev ; \
  /robust_install.sh ksh ; \
  /robust_install.sh gawk ; \
  /robust_install.sh lsb-core ; \
  /robust_install.sh libusb-dev ; \
  /robust_install.sh uuid ; \
  /robust_install.sh uuid-dev ; \
  /robust_install.sh uuid-runtime ; \
  /robust_install.sh symlinks ; \
  /robust_install.sh default-jre ; \
  /robust_install.sh git-filter-repo ; \
  /robust_install.sh gnuplot-nox ; \
  /robust_install.sh bbe ; \
  /robust_install.sh procmail ; \
  /robust_install.sh apache2 ; \
  /robust_install.sh apache2-utils ; \
  /robust_install.sh loggedfs ; \
  /robust_install.sh tcpdump ; \
  /robust_install.sh acl ; \
  /robust_install.sh libacl1-dev ; \
  /robust_install.sh attr ; \
  /robust_install.sh libattr1-dev ; \
  /robust_install.sh libxxhash-dev ; \
  /robust_install.sh libzstd-dev ; \
  /robust_install.sh liblz4-dev ; \
  /robust_install.sh libssl-dev ; \
  /robust_install.sh ip2route ; \
  /robust_install.sh isc-dhcp-client ; \
  /robust_install.sh s3backer ; \
}

RUN { \
  /robust_install.sh awscli ; \
  /robust_install.sh btrfs-progs ; \
  /robust_install.sh xfsprogs ; \
  /robust_install.sh f2fs-tools ; \
  /robust_install.sh maven ; \
}

# none of this other nonsense
RUN bash -c "cd /bin ; rm -f sh ; ln -sf bash sh"
RUN bash -c "echo 'export PAGER=less' >> /etc/profile"
RUN bash -c "echo 'export EDITOR=emacs' >> /etc/profile"

RUN mkdir -p /usr/local/install
RUN bash -c "echo /usr/local/install >> /etc/ld.so.conf"

RUN bash -c "ln -sf /usr/share/zoneinfo/America/Los_Angeles /etc/localtime"
RUN bash -c "echo America/Los_Angeles > /etc/timezone"
RUN bash -c "dpkg-reconfigure -f noninteractive tzdata"

# RUN bash -c 'echo LC_ALL="en_US.UTF-8" >> /etc/default/locale'

RUN bash -c 'cat /etc/ssh/sshd_config | grep -v "^Port " | grep -v PermitRootLogin | grep -v AcceptEnv | (cat ; echo "Port 3022" ; echo "PermitRootLogin yes" ; echo "AcceptEnv LANG LC_* TRUXTERM*") > /tmp/sshd_config ; mv -vf /tmp/sshd_config /etc/ssh/sshd_config'

## if you want rsync to be able to support case-insensitivity for windows users, you can use this patch
## but in most cases, it is not needed.
## RUN bash -c '(cd /usr/local/install && wget https://www.samba.org/ftp/rsync/rsync-3.2.3.tar.gz && wget https://www.samba.org/ftp/rsync/rsync-patches-3.2.3.tar.gz && tar -xpzvf rsync-3.2.3.tar.gz && tar -xpzvf rsync-patches-3.2.3.tar.gz && cd rsync-3.2.3 && patch -p1 <patches/ignore-case.diff && ./configure && make && make install)'

## install wsdd to make samba visible to windows10
## https://devanswers.co/discover-ubuntu-machines-samba-shares-windows-10-network/
RUN bash -c '(cd /usr/local/install && wget http://www.truxton.com/~trux/p/christgau_wsdd.zip || wget -O christgau_wsdd.zip https://github.com/christgau/wsdd/archive/master.zip)'
RUN bash -c '(cd /usr/local/install && unzip christgau_wsdd.zip && mv wsdd-master/src/wsdd.py wsdd-master/src/wsdd && cp wsdd-master/src/wsdd /usr/bin && cat wsdd-master/etc/systemd/wsdd.service | sed "s/^.*=nobody/; &/g" > /etc/systemd/system/wsdd.service)'
RUN bash -c 'systemctl daemon-reload ; systemctl enable wsdd'

## install nfs4j to provide nfs4
## https://github.com/gfi-centre-ouest/nfs4j-daemon
RUN bash -c '(cd /usr/local/install && wget http://www.truxton.com/~trux/p/gfi-centre-ouest_nfs4j-daemon-0.6.0.tar.gz || wget -O gfi-centre-ouest_nfs4j-daemon-0.6.0.tar.gz https://github.com/gfi-centre-ouest/nfs4j-daemon/archive/0.6.0.tar.gz)'
RUN bash -c '(cd /usr/local/install && tar -xpzvf gfi-centre-ouest_nfs4j-daemon-0.6.0.tar.gz && cd nfs4j-daemon-0.6.0 && mvn clean verify && cp -vf dist/nfs4j-daemon.jar /usr/local/bin)'

RUN mkdir -p /usr/local/etc
RUN mkdir -p /root/.ssh
RUN echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAADEwx3O9uoS6FZw6S4pRZHvjnlcqvCdarC/jp50WU3ed7JDIxA5Hun9xuHkggnAzzjKKTg13TgXYN0xjhRrZ9tXIoUPL3lGKHX8TM5cjGcm4GgMqBujjbx8R5VMAhgdTgzhJFrBATcUj8mNWjlPYEtUT2KJN5b5HBGe3aIazG8ZEf+Fb5Skc85jX66CFARY3u1wKrCOKCm4ydGoq5kDy/YazCsZJMgfoCr5oKGRQCc23MgwKwaf60smtCoAta1emLShZlGdntotJqUNoy5FxBWokLjmFjhXm0iHfVLKtEEzIvw3tj/UszK7QwwbfXXJJ2hnfq0HkLxmb1OaudSIVQu4YY3yBHCOJSD31B4AceygxdKTUUrzQ5Gudnlvn29RAYQsZGAgUrphWG09fGmcbmZX8u0FobrQdyu2Vo3+6zaS7jM1E3bxwvKegz6xKxyjWZfHinwojXpjEut0KfXJOFoPN7QGSyfus169MT1SrKzBz0euF2SgUQcDEgLSRK+me2CDddXOQ4ALMpVeuGz1SknV/yck0tCdLPZwAlS6eXzQMWAZjCmgsuXWuyPRqup1ufrwtjQS9owgypa5eoGdfhCzJd5HrMtGcgM+Yaci6yecF6Jaki96dS9xOkRFGN0bpKMu02U3DKWQdeB8/R0iVToEIn7pr2tGU8XTOn+yomG3m2trCjTvyuukX4mlIixGmXDeq0AYlpNBAUY1FSJE4Uq7c389hxsxJJMkip/f8sIacdTCSuYFN0HklgiGhGx7k2c9jYj5QjRji7k8UH+AMsgsbnfCJz72+qEUt3RR2QbC7hu9lM8bi1+rbTZFkFeY9mmIxjIKTfn70mDd8C8IG69isQ5ailcO7Anm6RfWIHO7qnqK9nczx/dNXPcwI0CPHYTfnVzqAQ6j4IsTT+8kOenJAYXUoVAgqCqllk35//DNLsyRnm91A8M2XnrNkC9xXCbrnRpnjYU4ioC9Dw0OHmPlWiF8hsLY1soEjghty5p/jx8h7brR4xkQiwe1J0k2TogrJYEDj8Et+3tGZx37CmtBkFOO/k= truxdesk" >> /root/.ssh/authorized_keys

RUN groupadd --gid 1000 local
RUN useradd --create-home --uid 1000 --gid 1000 local
RUN bash -c 'adduser local sudo'

RUN mkdir -p /home/local/.ssh
RUN cp /root/.ssh/authorized_keys /home/local/.ssh
RUN chown -R local /home/local/.ssh

RUN bash -c 'echo "" >> /etc/motd'
RUN bash -c 'echo "bosfs: docker container" >> /etc/motd'
RUN bash -c 'echo "" >> /etc/motd'


RUN { \
      echo '' ; \
      echo '  ScriptAlias /cgi-bin/ /var/www/cgi/' ; \
      echo '  ScriptAlias /cgi/ /var/www/cgi/' ; \
      echo '  <Directory "/var/www/cgi">' ; \
      echo '    AllowOverride None' ; \
      echo '    Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch' ; \
      echo '    AddHandler cgi-script .cgi' ; \
      echo '    Require all granted' ; \
      echo '    Allow from all' ; \
      echo '  </Directory>' ; \
      echo '  Alias /bosfs /mnt/bosfs' ; \
      echo '  <Directory "/mnt/bosfs">' ; \
      echo '    AllowOverride None' ; \
      echo '    Options -ExecCGI +MultiViews +Indexes +SymLinksIfOwnerMatch' ; \
      echo '    Require all granted' ; \
      echo '    Allow from all' ; \
      echo '  </Directory>' ; \
      echo '' ; \
    } > /tmp/apache2.cgi.inject.temp

RUN sed -i '/DocumentRoot/r /tmp/apache2.cgi.inject.temp' /etc/apache2/sites-enabled/000-default.conf

RUN sed -i '/DocumentRoot/a ServerName bosfs\n' /etc/apache2/sites-enabled/000-default.conf

RUN a2enmod cgid

RUN bash -c 'echo "CGIDScriptTimeout 7200" >> /etc/apache2/mods-enabled/cgid.conf'

RUN service apache2 restart

RUN { \
      echo '#!/bin/bash' ; \
      echo ; \
      echo 'rm -rf /var/run/screen/S*' ; \
      echo ; \
      echo 'echo "" >> /etc/motd' ; \
      echo ; \
      echo 'export TZ="/usr/share/zoneinfo/America/Los_Angeles"' ; \
      echo ; \
      echo 'umask 0022' ; \
      echo 'chmod a+rwx /mnt/bosfs' ; \
      echo 'chmod +t /mnt/bosfs' ; \
      echo 'echo "user_allow_other" >> /etc/fuse.conf' ; \
      echo 'test -s /mnt/bosfs.git/smb.conf && cp -vf /mnt/bosfs.git/smb.conf /etc/samba' ; \
      echo 'test -s /mnt/bosfs.git/favicon.ico && cp -vf /mnt/bosfs.git/favicon.ico /var/www/html' ; \
      echo 'cp -vf /mnt/bosfs.git/index*.html /var/www/html' ; \
      echo 'echo "-----"' ; \
      echo 'service rsyslog start' ; \
      echo 'service ssh start' ; \
      echo 'service nmbd start' ; \
      echo 'service smbd start' ; \
      echo 'service apache2 start' ; \
      echo 'apache2ctl start' ; \
      echo 'systemctl enable wsdd' ; \
      echo 'rm -rf /tmp/.X*' ; \
      echo 'screen -S s3backer -m -d -ln -h 8192 -e^Oo /mnt/bosfs.git/s3backer_loop.sh'; \
      echo 'screen -S xfs -m -d -ln -h 8192 -e^Oo /mnt/bosfs.git/xfs_loop.sh'; \
      echo 'screen -S rsyncd -m -d -ln -h 8192 -e^Oo /mnt/bosfs.git/rsyncd_loop.sh'; \
      echo 'screen -S nfs4j -m -d -ln -h 8192 -e^Oo /mnt/bosfs.git/nfs4j_loop.sh'; \
      echo 'screen -S wsdd -m -d -ln -h 8192 -e^Oo /mnt/bosfs.git/wsdd_loop.sh'; \
      echo 'rpcinfo -p' ; \
      echo 'while true ; do' ; \
      echo '  date' ; \
      echo '  sleep 64' ; \
      echo '  test -s /mnt/bosfs.git/ssh_host_rsa_key || cp -vf /etc/ssh/ssh_host_* /mnt/bosfs.git/' ; \
      echo 'done' ; \
    } > /init.sh && chmod a+rx /init.sh

COPY Dockerfile /

## CMD /init.sh
