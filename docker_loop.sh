#!/bin/bash

## to be invoked by rc.local or whatever...

export ROOTDIR="$( cd "$( dirname "$( readlink -e "${BASH_SOURCE[0]}" )" )" && /bin/pwd )"

cd ${ROOTDIR}

while true ; do
    docker ps | grep -w bosfs || docker rm bosfs
    docker wait bosfs

    service rpcbind stop

    mkdir -p docker_build_dir
    cp -vf Dockerfile docker_build_dir
    docker build -f Dockerfile -t bosfs:latest docker_build_dir 2>&1 | tee docker_build_dir/docker.build.log

    ## --privileged might not be necessary if you uncomment #user_allow_other from /etc/fuse.conf on the host
    ## docker run --rm --privileged --cap-add SYS_ADMIN --cap-add SYS_PTRACE --cap-add MKNOD --device=/dev/fuse --name bosfs -p 80:80 -p 443:443 -p 137:137 -p 138:138 -p 139:139 -p 445:445 -p 3022:3022 -p 873:873 --name bosfs -h bosfs -v `/bin/pwd`:/mnt/bosfs.git --dns 1.0.0.1 bosfs:latest /init.sh
    docker run --rm --privileged --net=host --device=/dev/fuse --name bosfs --name bosfs -h bosfs -v `/bin/pwd`:/mnt/bosfs.git --dns 1.0.0.1 bosfs:latest /init.sh
    
    date
    sleep 2
done
