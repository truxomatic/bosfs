#!/bin/bash

## to be run as root

export ROOTDIR="$( cd "$( dirname "$( readlink -e "${BASH_SOURCE[0]}" )" )" && /bin/pwd )"

while true ; do
  if mount | grep bosfs | grep xfs ; then
    if test -d /mnt/bosfs ; then
      cd /mnt/bosfs
      java -jar /usr/local/bin/nfs4j-daemon.jar --permission-type=UNIX --uid=1000 --gid=1000
    fi
  fi
  date
  sleep 2
done

## remote mount with something like this in /etc/fstab
## 10.28.0.222:/         /mnt/bosfs     nfs4 rw,soft,timeo=600,retrans=2,noresvport,port=2049,rsize=1048576,wsize=1048576,nfsvers=4.1,tcp 0 2
## use "apt install nfs-common ; mkdir -p /mnt/bosfs ; mount /mnt/bosfs ; chmod a+rwx /mnt/bosfs"
