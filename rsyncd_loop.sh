#!/bin/bash

## to be run as root

export ROOTDIR="$( cd "$( dirname "$( readlink -e "${BASH_SOURCE[0]}" )" )" && /bin/pwd )"

while true ; do
    cd ${ROOTDIR}
    if test -s /usr/local/bin/rsync ; then
	/usr/local/bin/rsync --log-file=/var/log/rsyncd.log --daemon --no-detach --port=873 --config=rsyncd.conf
    else
	rsync --log-file=/var/log/rsyncd.log --daemon --no-detach --port=873 --config=rsyncd.conf
    fi
  sleep 1
done


## client usage is like this :
## rsync --list-only rsync://bosfs/bosfs
## rsync -zaP rsync://bosfs/bosfs/somefile .


