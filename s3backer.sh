#!/bin/bash

export GITDIR=$1
export CACHEDIR=$2

if test -z "$CACHEDIR" ; then
    if test -d /mnt/bosfs.git ; then
	export CACHEDIR=/mnt/bosfs.git
    else
	echo "ERROR: must specify CACHEDIR on command line if /mnt/bosfs.git does not exist"
	exit 1
    fi
fi
if ! test -d "$CACHEDIR" ; then
    echo "ERROR: CACHEDIR ($CACHEDIR) does not exist"
    exit 1
else
    echo "CACHEDIR is ${CACHEDIR}"
fi
if ! test -d "$GITDIR" ; then
    echo "ERROR: GITDIR ($GITDIR) does not exist"
    exit 1
else
    echo "GITDIR is ${GITDIR}"
fi

if ! which aws ; then
    echo "ERROR: aws cli missing"
    exit 1
fi

if ! which uuid ; then
    echo "ERROR: uuid missing"
    exit 1
fi

if ! which calc ; then
    echo "ERROR: apcalc missing"
    exit 1
fi

if ! which s3backer ; then
    echo "ERROR: s3backer missing"
    exit 1
fi

export AWS_REGION=us-west-1

export COMPRESS_LEVEL=1

export VIRTUAL_SIZE=1P

export BLOCK_SIZE=2M

export AVAILABLE_DISK_SPACE=`df --si --output=avail ${CACHEDIR} | tail -1 | awk '{print $1}'`
echo "AVAILABLE_DISK_SPACE = ${AVAILABLE_DISK_SPACE}"

export AVAILABLE_DISK_BYTES=`echo ${AVAILABLE_DISK_SPACE} | sed "s=K.*=* 10^3=g" | sed "s=M.*=* 10^6=g" | sed "s=G.*=* 10^9=g" | sed "s=T.*=* 10^12=g" | sed "s=P.*=* 10^15=g" | calc -p | awk '{print $1}'`

echo "AVAILABLE_DISK_BYTES = ${AVAILABLE_DISK_BYTES}"

export BYTES_PER_BLOCK=`echo ${BLOCK_SIZE} | sed "s=K.*=* 2^10=g" | sed "s=M.*=* 2^20=g" | sed "s=G.*=* 2^30=g" | sed "s=T.*=* 2^40=g" | sed "s=P.*=* 2^50=g" | calc -p | awk '{print $1}'`
echo "BYTES_PER_BLOCK = ${BYTES_PER_BLOCK}"

export VIRTUAL_SIZE_BYTES=`echo ${VIRTUAL_SIZE} | sed "s=K.*=* 2^10=g" | sed "s=M.*=* 2^20=g" | sed "s=G.*=* 2^30=g" | sed "s=T.*=* 2^40=g" | sed "s=P.*=* 2^50=g" | calc -p | awk '{print $1}'`
echo "VIRTUAL_SIZE_BYTES = ${VIRTUAL_SIZE_BYTES}"

TOTAL_NUM_BLOCKS=$((${VIRTUAL_SIZE_BYTES} / ${BYTES_PER_BLOCK}))
echo "TOTAL_NUM_BLOCKS = ${TOTAL_NUM_BLOCKS}"

if test -s ${GITDIR}/.passwd-s3 ; then
  cp -vf ${GITDIR}/.passwd-s3 /root/.passwd-s3
fi

if test -s ${GITDIR}/.bosfs.encryption.password ; then
  cp -vf ${GITDIR}/.bosfs.encryption.password /root/.bosfs.encryption.password
else
  ## create zero byte file just to indicate the encryption password is empty
  touch ${GITDIR}/.bosfs.encryption.password
fi
## uncomment this if you want to create a random encryption password
# if ! test -s /root/.bosfs.encryption.password ; then
#   uuid -F SIV > /root/.bosfs.encryption.password
#   cp -vf /root/.bosfs.encryption.password ${GITDIR}/.bosfs.encryption.password
# fi
if test -s /root/.bosfs.encryption.password ; then
  cp -vf /root/.bosfs.encryption.password ${GITDIR}/.bosfs.encryption.password
  export ENCRYPT_OPTION=" --encrypt --passwordFile=/root/.bosfs.encryption.password "
else
  export ENCRYPT_OPTION=" "
fi

if test -s ${GITDIR}/.bosfs.prefix ; then
  cp -vf ${GITDIR}/.bosfs.prefix /root/.bosfs.prefix
fi
if test -s /root/.bosfs.prefix ; then
  export PREFIX=`cat /root/.bosfs.prefix | awk '{print $1}' | grep . | head -1`
else
  export PREFIX=bosfs
  echo ${PREFIX} > /root/.bosfs.prefix
  cp -vf /root/.bosfs.prefix ${GITDIR}/.bosfs.prefix
fi

if test -s ${GITDIR}/.bosfs.bucketname ; then
  cp -vf ${GITDIR}/.bosfs.bucketname /root/.bosfs.bucketname
fi
if test -s /root/.bosfs.bucketname ; then
  export BUCKETNAME=`cat /root/.bosfs.bucketname | awk '{print $1}' | grep . | head -1`
else
  export BUCKETNAME=bosfs-`uuid | md5sum | awk '{print $1}' | cut --bytes="1-12"`
  echo ${BUCKETNAME} > /root/.bosfs.bucketname
  cp -vf /root/.bosfs.bucketname ${GITDIR}/.bosfs.bucketname
fi

if test -s /root/.passwd-s3 ; then
  mkdir -p /mnt/s3backer

  cp -vf /var/www/html/index.redirect.html /var/www/html/index.html

  export AWS_ACCESS_KEY_ID=`cat /root/.passwd-s3 | sed "s=:.*==g" | awk '{print $1}' | grep . | head -1`
  export AWS_SECRET_ACCESS_KEY=`cat /root/.passwd-s3 | sed "s=.*:==g" | awk '{print $1}' | grep . | head -1`

  echo "## create the bucket if it does not already exist : ${BUCKETNAME}"
  aws s3api create-bucket --bucket ${BUCKETNAME} --region ${AWS_REGION} --create-bucket-configuration LocationConstraint=${AWS_REGION}
  export AWS_RESULT=$?
  echo "AWS_RESULT = ${AWS_RESULT}"

  if test -s ${CACHEDIR}/cachefile ; then
    echo "using existing cache file:"
    ls -l ${CACHEDIR}/cachefile
    export BLOCK_CACHE_SIZE=`cat ${GITDIR}/.bosfs.block.cache.size`
  else
    rm -f ${CACHEDIR}/cachefile
    if test -s ${GITDIR}/s3backer.sh ; then
	export DISK_CACHE_NUM_BYTES_TO_USE=$((AVAILABLE_DISK_BYTES * 3 / 4))
    else
	export DISK_CACHE_NUM_BYTES_TO_USE=$((AVAILABLE_DISK_BYTES * 99 / 100))
    fi
    echo "DISK_CACHE_NUM_BYTES_TO_USE = ${DISK_CACHE_NUM_BYTES_TO_USE}"
    BLOCK_CACHE_SIZE=$((${DISK_CACHE_NUM_BYTES_TO_USE} / ${BYTES_PER_BLOCK}))
    echo ${BLOCK_CACHE_SIZE} > ${GITDIR}/.bosfs.block.cache.size
  fi

  echo "BLOCK_CACHE_SIZE = ${BLOCK_CACHE_SIZE}"
  export CACHEOPTS=" --blockCacheFile=${CACHEDIR}/cachefile --blockCacheSize=${BLOCK_CACHE_SIZE} --blockCacheThreads=4 "

  echo "## reset mounted flag..."
  s3backer --reset-mounted-flag -f --accessFile=/root/.passwd-s3 --size=${VIRTUAL_SIZE} ${CACHEOPTS} --blockSize=${BLOCK_SIZE} --compress=${COMPRESS_LEVEL} ${ENCRYPT_OPTION} --prefix=${PREFIX} --filename=bosfs --region=${AWS_REGION} --timeout=64 --blockCacheWriteDelay=120000 ${BUCKETNAME}

  echo "## running s3backer..."
  s3backer -f --accessFile=/root/.passwd-s3 --size=${VIRTUAL_SIZE} ${CACHEOPTS} --blockSize=${BLOCK_SIZE} --compress=${COMPRESS_LEVEL} ${ENCRYPT_OPTION} --prefix=${PREFIX} --filename=bosfs --region=${AWS_REGION} --timeout=64 --blockCacheWriteDelay=120000 ${BUCKETNAME} /mnt/s3backer
else
  sleep 1
  mkdir -p /tmp/waitforkey
  chmod a+rwx /tmp/waitforkey
  cp -vf /var/www/html/index.need_setup.html /var/www/html/index.html
  echo "# wait for AWS keys (initialized via web cgi)"
  inotifywait --timeout 32 -e modify -e attrib -e create -e delete /tmp/waitforkey/

  if test -s /tmp/waitforkey/.passwd-s3 ; then
    if test -d ${GITDIR}/ ; then
      mv -vf /tmp/waitforkey/.passwd-s3 ${GITDIR}/
    fi
  fi
fi


