#!/bin/bash

export ROOTDIR="$( cd "$( dirname "$( readlink -e "${BASH_SOURCE[0]}" )" )" && /bin/pwd )"

if test -d /mnt/s3cache ; then
    export CACHEDIR=/mnt/s3cache
else
    export CACHEDIR=${ROOTDIR}
fi

while true ; do

    cp -v ${ROOTDIR}/s3backer.sh /tmp/s3backer.sh
    sh /tmp/s3backer.sh ${ROOTDIR} ${CACHEDIR}

    date
    sleep 1

done
