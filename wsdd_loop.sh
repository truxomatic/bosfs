#!/bin/bash

## to be run as root

export ROOTDIR="$( cd "$( dirname "$( readlink -e "${BASH_SOURCE[0]}" )" )" && /bin/pwd )"

while true ; do
  cd ${ROOTDIR}
  /usr/bin/wsdd -v
  sleep 1
done


## client usage is like this :
## rsync --list-only rsync://bosfs/bosfs
## rsync -zaP rsync://bosfs/bosfs/somefile .


