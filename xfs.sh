#!/bin/bash

if test -s /mnt/s3backer/bosfs ; then
  echo "ready: /mnt/s3backer/bosfs"
  mkdir -p /mnt/bosfs
  if mount | grep -v /mnt/bosfs.git | grep /mnt/bosfs | grep xfs ; then
    echo "/mnt/bosfs is already mounted"
    sleep 128
  else
    echo "mounting /mnt/bosfs"
    if mount -o loop,rw /mnt/s3backer/bosfs /mnt/bosfs ; then
      echo "successfully mounted /mnt/bosfs"
      chmod a+rwx /mnt/bosfs
      service smbd restart
      sleep 128
    else
      echo "failed to mount /mnt/bosfs"
      if test -s /mnt/bosfs.git/.bosfs.xfs.created.flag ; then
        echo "already created a filesystem.  cowardly refusing to create a new one."
        echo "if needed, delete /mnt/bosfs.git/.bosfs.xfs.created.flag"
        echo "running xfs_repair (fsck) instead..."
        xfs_repair -v -v -v -P -L /mnt/s3backer/bosfs
        sleep 8
      else
        echo "trying to create new xfs filesystem on s3backer..."
        if mkfs.xfs -L bosfs -K /mnt/s3backer/bosfs ; then
          echo "created xfs filesystem."
          date > /mnt/bosfs.git/.bosfs.xfs.created.flag
        else
          echo "ERROR: creating xfs filesystem failed."
          sleep 128
        fi
      fi
    fi
  fi
else
  echo "waiting for s3backer to create /mnt/s3backer/bosfs ..."
fi

sleep 8
