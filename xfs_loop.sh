#!/bin/bash

export ROOTDIR="$( cd "$( dirname "$( readlink -e "${BASH_SOURCE[0]}" )" )" && /bin/pwd )"

while true ; do

    cp -v ${ROOTDIR}/xfs.sh /tmp/xfs.sh
    sh /tmp/xfs.sh

    date
    sleep 1

done
